﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson15_03_Main_Menu
{
    public partial class MainForm : Form
    {
        Encoding encoding = Encoding.UTF32;
        Encoding[] encodings = {Encoding.ASCII, Encoding.Unicode, Encoding.UTF32, Encoding.UTF7, Encoding.UTF8, Encoding.BigEndianUnicode, Encoding.Default};
        string[] enc = { "ASCII", "Unicode", "UTF32", "UTF7", "UTF8", "BigEndianUnicode", "System Default" };
        private string fileName = string.Empty;
        public MainForm()
        {
            InitializeComponent();
            saveToolStripMenuItem.Enabled = false;
            toolStripComboBox.Items.AddRange(enc);
            toolStripComboBox.SelectedItem = enc[6];
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.DefaultExt = "txt";
            openFileDialog.Filter = "All files (*.*)|*.*|Text files(*.txt)|*.txt"; //описание + маска 
            openFileDialog.FilterIndex = 2; // считаются с 1
            if(openFileDialog.ShowDialog(this)==DialogResult.OK)
            {
                fileName = openFileDialog.FileName;
                saveToolStripMenuItem.Enabled = true;
                using (StreamReader reader = new StreamReader(openFileDialog.OpenFile(), getSelectedEncoding()))
                {

                    statusStrip.Text = fileName;
                    textBox.Text = reader.ReadToEnd();
                }  

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, getSelectedEncoding()))
            {
                writer.Write(textBox.Text);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.DefaultExt = "txt";
            saveFileDialog.Filter = "All files (*.*)|*.*|Text files(*.txt)|*.txt"; //описание + маска 
            saveFileDialog.FilterIndex = 2; // считаются с 1
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                fileName = openFileDialog.FileName;
                saveToolStripMenuItem.Enabled = true;
                using (StreamWriter writer = new StreamWriter(saveFileDialog.OpenFile(), getSelectedEncoding()))
                {
                    statusStrip.Text = "Axel Primeiro";
                    writer.Write(textBox.Text);
                }
            }
        }

        private void fontToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            fontDialog.Font = textBox.Font;
            if(fontDialog.ShowDialog(this)==DialogResult.OK)
            {
                textBox.Font = fontDialog.Font;
            }
        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog.Color = textBox.BackColor;
            if (colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                textBox.BackColor = colorDialog.Color;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox.Text = "";
            fileName = string.Empty;
            saveToolStripMenuItem.Enabled = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (textBox.Text != "" && MessageBox.Show("Do you want to save changes?", "File is changed", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                saveAsToolStripMenuItem_Click(sender, e);
                Close();
            }
            else
                Close();
        }
        private Encoding getSelectedEncoding()
        {
            for(int i =0;i<enc.Length; i++)
            {
                if (toolStripComboBox.SelectedItem.Equals(enc[i]))
                    return encodings[i];
            }
            return encodings[6];
        }

        private void toolStripButtonBookMode_Click(object sender, EventArgs e)
        {
            if (toolStripButtonBookMode.Pressed)
            {
                saveToolStripMenuItem.Enabled = false;
                saveAsToolStripMenuItem.Enabled = false;
                newToolStripMenuItem.Enabled = false;
                toolStripButtonNew.Enabled = false;
                toolStripButtonSaveAs.Enabled = false;
                textBox.ReadOnly = true;
            }


        }

        private void toolStripButtonTextMode_Click(object sender, EventArgs e)
        {
            if (toolStripButtonTextMode.Pressed)
            {
                saveAsToolStripMenuItem.Enabled = true;
                newToolStripMenuItem.Enabled = true;
                toolStripButtonNew.Enabled = true;
                toolStripButtonSaveAs.Enabled = true;
                textBox.ReadOnly = false;
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Simple text editor \nLeonid Osym, 2018", "About", MessageBoxButtons.OK);
        }
    }
}
